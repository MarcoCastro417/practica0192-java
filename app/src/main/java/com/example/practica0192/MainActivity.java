package com.example.practica0192;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {
    private Button btnPulsar;
    private EditText txtNombre;
    private TextView lblSaludar;
    private Button btnLimpiar;
    private Button btnCerrar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        // Relacionar los objetos
        btnPulsar = (Button) findViewById(R.id.btnSaludar);
        txtNombre = (EditText) findViewById(R.id.txtNombre);
        lblSaludar = (TextView) findViewById(R.id.lblSaludo);
        btnLimpiar = (Button) findViewById(R.id.btnLimpiar);
        btnCerrar = (Button) findViewById(R.id.btnCerrar);
        // Codificar el evento click del botón
        btnPulsar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // Validar
                if (txtNombre.getText().toString().matches("")){
                    Toast.makeText(MainActivity.this, "Faltó capturar información", Toast.LENGTH_SHORT).show();
                }else{
                    String str = "Hola " + txtNombre.getText().toString() + ", ¿cómo estás?";
                    // Toast.makeText(MainActivity.this, str, Toast.LENGTH_SHORT).show();
                    lblSaludar.setText(str.toString());
                }
            }
        });
        btnLimpiar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                lblSaludar.setText(":: ::");
                txtNombre.setText("");
            }
        });
        btnCerrar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
    }
}